/*  

exact 정확히 그 주소를 입력해야 컴포넌트 렌더링
다른 방법

<Switch> 이용 (react-router-dom내부에서 import)
이용시 주의사항
params가 있는 컴포넌트가 위쪽으로 있어야함
반대일 경우 :exam을 입력해도 /content의 내용만 나오게 된다.
ex) 
path = "/about/:exam"
path = "/about"
(O)

<Switch>
    <Route exact path="/about/foo" component = {Content1} />
    <Route exact path="/about" component = {Content1} />
</Switch>

*/

import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Home, Regist, Posts, Login } from '../pages';

class App extends Component {

    render() {
        return (
            <div id="contents">
                <Route exact path="/" component = {Home} />
                <Route exact path="/regist" component = {Regist} />
                <Route exact path="/login" component = {Login} />
                <Route path="/posts" component={Posts}/>
            </div>
        );
    }

}

export default App;


/*
    root.js에서 <App /> 부분에 그려질 파일


    exact : 주어진 경로와 정확히 맞아 떨어져야지만 설정한 컴포넌트를 보여줄떄 사용
*/

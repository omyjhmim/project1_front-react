import React, { Component } from 'react';
/* 


<ul>
                <li><Link to={`${match.url}/1`}>Post #1</Link></li>
                <li><Link to={`${match.url}/2`}>Post #2</Link></li>
                <li><Link to={`${match.url}/3`}>Post #3</Link></li>
                <li><Link to={`${match.url}/4`}>Post #4</Link></li>
            </ul>

            <Route exact path = {match.url} render={()=>(<h3>Please select any post</h3>)} />
            <Route path={`${match.url}/:id`} component={Post}/>


<div className="User-list">
                    <p>{users.length!=0 ? <h1>Hello! Welcome~</h1> : <h1>Loading.. please wait!</h1>} </p>
       
                    <div className="usrs">{usersList}</div>
                </div>

*/
class Posts extends Component {
    
    
    constructor(props) {
        super(props);
        let user_token = "asdf";
        this.state = {
            users: [],
            getUsersData: () => {
                fetch('http://localhost:4000/api/user/list',{
                    method: "post", 
                    headers: {
                        'Content-Type': 'application/json',
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    body: JSON.stringify({
                        token: user_token
                    }),
                })
                .then(res => res.json())
                .then(data => this.setState({
                    users: data
                }));
                console.log("This state function");
            }
        }
        console.log("constructor !!");

        
    }

    // setState 가 호출되면 컴포넌트가 리렌더링 된다
    componentDidMount() {
        this.state.getUsersData();
    }

    
   
    
    render() {
        let {users} = this.state;
        let usersList = users.map((user) => (
            <div key={user.user_id} id={`user${user.user_id}`}>
                <h1>{`NO.: ${user.user_id}`}</h1>
                <p>{`이메일: ${user.email}`}</p>
                <p>{`이름: ${user.name}`}</p>
                <p>{`전화번호: ${user.phone_number}`}</p>
            </div>
        ));

        return (
            <div>
                <div className="user-list">
                    {usersList}
                </div>
            </div>
        );

        
    }
}


export default Posts;
/* 

Post 컴포넌트를 불러올때 ./Post 에서 바로 불러오는게 아닌 pages를 통해 볼러온다.

코드 스필리팅을 해보게 될텐데, 이 과정에서 페이지를 불러오는 방식이 통일 되어야 하기때문에 pages에서 불러오도록 함

match.url = 현재 라우트의 경로를 알려줌
이부분에서 그냥 to="/posts/1" 로 설정해도 된다만
위의 코드처럼 {`${match.url`}를 이용
} 하는것이 나중에 현재 라우트 경로가 바뀌게 되면 자동으로 바뀌게 되는 장점이있다.

하단 라우트 설정에 첫번쨰 라우트는 match.url로 설정되어있는데 이건 아무런 포스트 id가 주어지지않았을때이다.
여기서 component 대신 render가 사용 되었는데. 이 render는 인라인 렌더링을 가능하게 해준다.
두번쨰 라우터에서는 현재 라우트주소에 :id가 붙을시 Post 컴포넌트를 보여주기 설정하였다.


*/
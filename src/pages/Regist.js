/*  

#5 라우트 파라미터 설정법

중 1번 params 이용하는 법

:banana 의 형식을 가지고있다.

App.js
path에 /content1/:exam/으로 하고 밑에 매개변수에 {match} 추가 후 
match.params.exam을 출력하면 잘 나온다.





*/

import React, { Component } from "react";

/*

#5 라우트 파라미터 설정
1) params 사용법 예제

const Content1 = ({match}) => {
    return (
        <div>
            <h2>Content1 {match.params.exam} </h2>
        </div>
    );
}
*/
/*

import queryString from 'query-string';

const Content1 = ({location, match}) => {

    const query = queryString.parse(location.search);
    //console.log(query);

    //query값에 따른 조건부 렌더링 해보기 start

    const omygirl = query.omygirl === 'mimi';

    //query값에 따른 조건부 렌더링 해보기 end

    return (
        <div>
            <h2>Content1 {match.params.exam} </h2>
            { 조건부 렌더링 법  omygirl && 'omygirl: mimi'} 사랑해
            
        </div>
    );
}
*/
// 위의 예제 입력 후 
// /content1/foo?omygirl=mimi
// 경로로 접속해보기
// console 창에 object { omygirl: "mimi" } 정상 출력



/* 실제 에제 */
class Regist extends Component {
    

    render() {
        return(
            <div id = "regist_form">
        <div>
           <h1>회원가입</h1>
       </div>
   
       <form method="post" action="http://localhost:4000/api/user/register">
           <div className="input_regist_info">
               <label>이메일</label>
               <input type="email" id="e_mail" name="email"></input>
           </div>
           <div className="input_regist_info">
               <label>비밀번호</label>
               <input type="password" id="pw1" name="password"></input>
           </div>
           <div className="input_regist_info">
               <label>비밀번호 확인</label>
               <input type="password" id="pw2" name="password_confirm"></input>
           </div>
           <div className="input_regist_info">
               <label>이름</label>
               <input type="text" id="name" name="realname"></input>
           </div>
           <div className="input_regist_info">
               <label>전화번호</label>
               <input type="tel" id="tel_number" name="tel_number"></input>
           </div>
           <div className="input_regist_info">
               <label>생년월일</label>
               <input type="date" id="birthday" name="birthday"></input>
           </div>
           <div className="input_regist_button">
               <div>
                   <input value="취소" type="button" id="cancel_button"></input>
                   <input value="가입" type="submit" id="regist_button"></input>
               </div>
           </div>
       </form>
   </div>
        );
    }
}

export default Regist;
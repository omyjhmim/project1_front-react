/* 
    컴포넌트를 불러와서 한 파일로 내보낼 수 있게 해주는 인텍스 파일
*/
export { default as Home } from './Home';
export { default as Regist } from './Regist';
export { default as Posts } from './Posts';
export { default as Post } from './Post';
export { default as Login } from './Login';

/*

주소에 아무런 path입력도 주어지지않았을때
기본적으로 보여지는 라우트

*/

import React from 'react';


const Home = () => {
    return (
        <h2>홈</h2>
    );
}

export default Home;
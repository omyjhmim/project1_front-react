import React,  { Component } from 'react';



class Login extends Component {

    
    render() {

        return (
            <div>
                <div>
                    <h1>로그인</h1>
                </div>
            
                <form method="post" action="http://localhost:4000/api/user/login">
                    <div class="input_regist_info">
                        <label for="e_mail">이메일</label>
                        <input type="email" id="login_e_mail" name="user_email"></input>
                    </div>
                    <div class="input_regist_info">
                        <label for="pw1">비밀번호</label>
                        <input type="password" id="login_pw1" name="password"></input>
                    </div>
                    <input value="로그인" type="submit" id="login-btn"></input>
                </form>
            </div>    
            
        );
    }

}



export default Login;

/*  

class Login extends Component {

    
    render() {
        return (
            <div>
                <div>
                    <h1>로그인</h1>
                </div>
            
                <form method="post" action="http://localhost:4000/login">
                    <div class="input_regist_info">
                        <label for="e_mail">전화번호</label>
                        <input type="text" id="login_e_mail" name="phone"></input>
                    </div>
                    <div class="input_regist_info">
                        <label for="pw1">비밀번호</label>
                        <input type="password" id="login_pw1" name="password"></input>
                    </div>
                    <input value="로그인" type="submit" id="login-btn"></input>
                </form>
            </div>    
            
        );
    }

}
*/
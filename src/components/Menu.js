import React from 'react';
import { NavLink } from 'react-router-dom';

// Link 사용법 Root.js에 import후 <Menu /> 추가 필요
const Menu = () => {


    const activeStyle = {
        color: '#fff',
        backgroundColor: '#FAAC58'
    }
    return (

        <div id="menu-items">
            <ul>
                <li><NavLink exact to ="/" activeStyle={activeStyle} onClick={tmp}>홈</NavLink></li>
                <li><NavLink to ="/regist" activeStyle={activeStyle}  onClick={tmp}>회원가입</NavLink></li>
                <li><NavLink to ="/login" activeStyle={activeStyle} onClick={tmp}>로그인</NavLink></li>
                <li><NavLink to ="/posts" activeStyle={activeStyle} onClick={tmp}>게시판</NavLink></li>
            </ul>
        </div>

    );
}

const tmp = () => {
    document.getElementById("aside-menu-btn").click();
}

/*  


<li><Link to ="/">홈</Link></li>
<li><Link to ="/content1">오마이걸</Link></li>
<li><Link to ="/content1/mimi">오마이걸 미미</Link></li>



NavLink Link와 비슷하지만 설정한 URL가 활성화 되면 특정 스타일, 또는 클래스를 지정할 수 있다.
activestyle={}
activeClassName = {} 으로 사용
*/





export default Menu;
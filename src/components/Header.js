import React from 'react';


const Header = () => {

    return (
        <div id = "header">
            <h2>용돈관리</h2>
            {/* React에서 클릭 이벤트 처리하기 */}
            <div id="aside-menu-btn" onClick={activeMenuButton}>
                <img src={require('../lib/menuicon.png')} alt = "Menu"/>
            </div>
        </div>
    );

}

let asideMenuClicked = true;

const activeMenuButton = () => {
    if(asideMenuClicked) {
        asideMenuClicked = false;
        document.getElementById("menu-items").classList.add("activeMenuButton");
        
    } else {
        asideMenuClicked = true;
        document.getElementById("menu-items").classList.remove("activeMenuButton");
        
    }
    
}

export default Header;
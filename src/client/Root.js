import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import App from '../shared/App';
import Menu from '../components/Menu';
import Header from '../components/Header';
const Root = () => {
    return (
        <BrowserRouter>
            <Header />
            <Menu />
            <App />
        </BrowserRouter>
    );
}
export default Root;